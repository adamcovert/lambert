var ready = function ( fn ) {
  // Sanity check
  if ( typeof fn !== 'function' ) return;

  // If document is already loaded, run method
  if ( document.readyState === 'interactive' || document.readyState === 'complete' ) {
    return fn();
  }

  // Otherwise, wait until document is loaded
  document.addEventListener( 'DOMContentLoaded', fn, false );
};

ready(function () {

  // Turn on Svg4Everybody
  svg4everybody();


  function $$(selector, context) {
    context = context || document;
    var elements = context.querySelectorAll(selector);
    return Array.prototype.slice.call(elements);
  };

  // Open and close mobile menu
  var burgers = $$('.hamburger');

  for (var i = 0; i < burgers.length; i++) {
    var burger = burgers[i];
    burger.addEventListener('click', showBurgerTarget);

    function showBurgerTarget() {
      var targetId = this.getAttribute('data-target-id');
      var targetClassToggle = this.getAttribute('data-target-class-toggle');

      if (targetId && targetClassToggle) {
        this.classList.toggle('hamburger--is-active');
        document.getElementById(targetId).classList.toggle(targetClassToggle);

        if (document.getElementById(targetId).classList.contains('mobile-menu--is-shown')) {
          document.querySelector('body').classList.add('prevent-scroll');
        } else {
          document.querySelector('body').classList.remove('prevent-scroll');
        }
      }
    }
  };

  /**
   * NodeList.prototype.forEach() polyfill
   * https://developer.mozilla.org/en-US/docs/Web/API/NodeList/forEach#Polyfill
   */
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;
      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  }

  // Show and close sublist navigation
  var mobileNavBtns = document.querySelectorAll('.mobile-menu__nav-btn');

  mobileNavBtns.forEach(function (btn) {
    btn.addEventListener('click', function () {
      var parent = btn.parentElement.parentElement;
      var parentElems = btn.parentElement.parentElement.children;

      for (var i = 0; i < parentElems.length; i++) {
        if (parentElems[i].classList.contains('mobile-menu__nav-sublist')) {
          parent.classList.toggle('mobile-menu__nav-item--is-active');
          parentElems[i].classList.toggle('mobile-menu__nav-sublist--is-open');
        }
      }
    });
  });

});