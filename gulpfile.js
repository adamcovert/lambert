'use strict';

// Global
const gulp = require('gulp');
const projectConfig = require('./projectConfig.json');
const dirs = projectConfig.directories;
const lists = getFilesList(projectConfig);
// console.log(lists);

const gulpSequence = require('gulp-sequence');
const browserSync = require('browser-sync').create();
const gulpIf = require('gulp-if');
const debug = require('gulp-debug');
const del = require('del');
const plumber = require('gulp-plumber');
const fileinclude = require('gulp-file-include');
const size = require('gulp-size');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const fs = require('fs');
const newer = require('gulp-newer');
const replace = require('gulp-replace');
const rigger = require('gulp-rigger');
const wait = require('gulp-wait');

const pug = require('gulp-pug');
const htmlbeautify = require('gulp-html-beautify');

// Sass
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const cleanss = require('gulp-cleancss');

// PostCSS
const postcss = require('gulp-postcss');
const autoprefixer = require("autoprefixer");
const mqpacker = require("css-mqpacker");
const objectFitImages = require('postcss-object-fit-images');
const inlineSVG = require('postcss-inline-svg');
const sorting = require('postcss-sorting');
const perfectionist = require('perfectionist');
const atImport = require("postcss-import");

// Svg Sprite
const svgstore = require('gulp-svgstore');
const svgmin = require('gulp-svgmin');
const cheerio = require('gulp-cheerio');

// Png Sprite
const spritesmith = require('gulp.spritesmith');
const buffer = require('vinyl-buffer');
const merge = require('merge-stream');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');

// Javascript
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

// NODE_ENV=production gulp
const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

const sortingOptions = require('./sortingOptions.json');

function reload (done) {
  browserSync.reload();
  done();
}

let onError = function(err) {
    notify.onError({
      title: 'Error in ' + err.plugin,
    })(err);
    this.emit('end');
};

// function fileExist(path) {
//   const fs = require('fs');
//   try {
//     fs.statSync(path);
//   } catch(err) {
//     return !(err && err.code === 'ENOENT');
//   }
// }

function fileExist(filepath){
  let flag = true;
  try{
    fs.accessSync(filepath, fs.F_OK);
  }catch(e){
    flag = false;
  }
  return flag;
}



let postCssPlugins = [
  autoprefixer(),
  inlineSVG(),
  objectFitImages(),
  sorting(sortingOptions),
  mqpacker({
    sort: true
  }),
  atImport(),
  perfectionist({
    indentSize: 2,
    trimLeadingZero: false,
    maxSelectorLength: 1
  })
];


let message = '/*!\n * Attention! This file is generated automatically.\n * Do not write anything here manually, all such edits will be lost.\n */\n\n';

let styleImports = message;
lists.css.forEach(function (blockPath) {
  styleImports += '@import \''+ blockPath +'\';\n';
});
fs.writeFileSync(dirs.source + 'scss/style.scss', styleImports);

let pugMixins = '';
lists.pug.forEach(function(blockPath) {
  pugMixins += 'include '+blockPath+'\n';
});
fs.writeFileSync(dirs.source + 'pug/mixins.pug', pugMixins);

// Clean
gulp.task('clean', function () {
  console.log('---------- Clean the dist folder');
  return del(dirs.build + '/**/*')
});



// HTML
gulp.task('html', function () {
  console.log('---------- Compiling Pug files')
  return gulp.src(dirs.source + '/*.pug')
    .pipe(plumber({
      errorHandler: function(err) {
        notify.onError({
          title: 'Pug compilation error',
          message: err.message
        })(err);
        this.emit('end');
      }
    }))
    .pipe(pug())
    .pipe(htmlbeautify({
      indent_size: 2,
      unformatted: [
        'abbr', 'area', 'b', 'bdi', 'bdo', 'br', 'cite',
        'code', 'data', 'datalist', 'del', 'dfn', 'em', 'embed', 'i', 'ins', 'kbd', 'keygen', 'map', 'mark', 'math', 'meter',
        'object', 'output', 'progress', 'q', 'ruby', 's', 'samp', 'small',
         'strong', 'sub', 'sup', 'template', 'time', 'u', 'var', 'wbr', 'text',
        'acronym', 'address', 'big', 'dt', 'ins', 'strike', 'tt'
      ]
    }))
    .pipe(gulp.dest(dirs.build));
});



// SASS
gulp.task('style', function () {
  console.log('---------- Generate Style');
  return gulp.src(dirs.source + '/scss/style.scss')
    .pipe(plumber({
      errorHandler: function(err) {
        notify.onError({
          title: 'Styles compilation error',
          message: err.message
        })(err);
        this.emit('end');
      }
    }))
    .pipe(wait(100))
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(debug({title: "scss:"}))
    .pipe(sass())
    .pipe(postcss(postCssPlugins))
    .pipe(gulpIf(!isDevelopment, cleanss()))
    .pipe(rename('style.min.css'))
    .pipe(gulpIf(isDevelopment, sourcemaps.write('/')))
    .pipe(size({
      title: 'Размер',
      showFiles: true,
      showTotal: false,
    }))
    .pipe(gulp.dest(dirs.build + '/css'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});



// CSS copying
gulp.task('copy:css', function(callback) {
  if(projectConfig.copiedCss.length) {
    return gulp.src(projectConfig.copiedCss)
      .pipe(postcss(postCssPlugins))
      .pipe(cleanss())
      .pipe(size({
        title: 'Размер',
        showFiles: true,
        showTotal: false,
      }))
      .pipe(gulp.dest(dirs.build + '/css'))
  } else {
    callback();
  }
});



// Images copying
gulp.task('copy:images', function () {
  console.log('---------- Copying images');
  return gulp.src(lists.img)
    .pipe(newer(dirs.build + '/img'))
    .pipe(size({
      title: 'Размер',
      showFiles: true,
      showTotal: false,
    }))
    .pipe(gulp.dest(dirs.build + '/img'));
});



// Videos copying
gulp.task('copy:videos', function () {
  console.log('---------- Copying videos');
  return gulp.src(dirs.source + '/videos/*.{mp4,ogv,webm}')
    .pipe(newer(dirs.build + '/videos'))
    .pipe(size({
      title: 'Размер',
      showFiles: true,
      showTotal: false,
    }))
    .pipe(gulp.dest(dirs.build + '/video'));
});



// Fonts Copying
gulp.task('copy:fonts', function () {
  console.log('---------- Copying fonts');
  return gulp.src(dirs.source + '/fonts/*.{ttf,woff,woff2,eot,svg}')
    .pipe(newer(dirs.build + '/fonts'))
    .pipe(size({
      title: 'Размер',
      showFiles: true,
      showTotal: false,
    }))
    .pipe(gulp.dest(dirs.build + '/fonts'));
});



// Javascript Files Copying
gulp.task('copy:js', function (callback) {
  if(projectConfig.copiedJs.length) {
    return gulp.src(projectConfig.copiedJs)
      .pipe(size({
        title: 'Размер',
        showFiles: true,
        showTotal: false,
      }))
      .pipe(gulp.dest(dirs.buildPath + '/js'));
  } else {
    callback();
  }
});



// Script
gulp.task('js', function (callback) {
  console.log('---------- Javascript concat/uglify');
  if(lists.js.length > 0) {
    return gulp.src(lists.js)
      .pipe(plumber({
        errorHandler: function(err) {
          notify.onError({
            title: 'Javascript concat/uglify error',
            message: err.message
          })(err);
          this.emit('end');
        }
      }))
      .pipe(concat('script.js'))
      .pipe(rename({ suffix: '.min' }))
      .pipe(gulpIf(!isDevelopment, uglify()))
      .pipe(size({
        title: 'Размер',
        showFiles: true,
        showTotal: false,
      }))
      .pipe(gulp.dest(dirs.build + '/js'));
  } else {
    callback();
  }
});


// SVG Sprite
let spriteSvgPath = dirs.source + dirs.blocksDirName + '/sprite-svg/svg/';
gulp.task('sprite:svg', function (callback) {
  if((projectConfig.blocks['sprite-svg']) !== undefined) {
    if(fileExist(spriteSvgPath) !== false) {
      console.log('---------- Create SVG sprite');
      return gulp.src(spriteSvgPath + '*.svg')
        .pipe(svgmin(function (file) {
          return {
            plugins: [{
              cleanupIDs: {
                minify: true
              }
            }]
          }
        }))
      .pipe(svgstore({ inlineSvg: true }))
      .pipe(cheerio({
        run: function($) {
          $('svg').attr('style',  'display:none');
        },
        parserOptions: {
          xmlMode: true
        }
      }))
      .pipe(rename('sprite-svg.svg'))
      .pipe(size({
        title: 'Размер',
        showFiles: true,
        showTotal: false,
      }))
      .pipe(gulp.dest(dirs.source + dirs.blocksDirName + '/sprite-svg/img/'));
    } else {
      console.log('There is no directory with .svg');
      callback();
    }
  } else {
    console.log('There is no SVG sprite in current project');
    callback();
  }
});



// PNG Sprite
let spritePngPath = dirs.source + dirs.blocksDirName + '/sprite-png/png/';
gulp.task('sprite:png', function (callback) {
  if((projectConfig.blocks['sprite-png']) !== undefined) {
    if(fileExist(spritePngPath) !== false) {
      console.log('---------- Create PNG sprite');
      del(dirs.source + dirs.blocksDirName + '/sprite-png/img/*.png');
      let fileName = 'sprite-png.png';
      let spriteData = gulp.src(spritePngPath + '*.png')
      .pipe(spritesmith({
        imgName: fileName,
        cssName: 'sprite-png.scss',
        padding: 4,
        imgPath: '../img/' + fileName
      }));

      let imgStream = spriteData.img
      .pipe(buffer())
      .pipe(imagemin({
        use: [pngquant()]
      }))
      .pipe(gulp.dest(dirs.source + dirs.blocksDirName + '/sprite-png/img/'));

      let cssStream = spriteData.css
      .pipe(gulp.dest(dirs.source + dirs.blocksDirName + '/sprite-png/'));

      return merge(imgStream, cssStream);
    } else {
      console.log('There is no directory with icons');
      callback();
    }
  } else {
    console.log('There is PNG sprite in current project');
    callback();
  }
});



gulp.task('serve', ['build'], function () {

  browserSync.init({
    server: dirs.build,
    startPath: 'index.html',
    open: false,
    port: 8080,
    logPrefix: 'project'
  });

  // HTML
  gulp.watch([
    '*.pug',
    dirs.blocksDirName + '/**/*.pug'
  ], {cwd: dirs.source}, ['watch:html']);

  // Styles
  gulp.watch([
    dirs.source + 'scss/style.scss',
    dirs.source + dirs.blocksDirName + '/**/*.scss',
    projectConfig.addCssBefore,
    projectConfig.addCssAfter,
  ], ['style']);

  gulp.watch(dirs.source + '/js/script.js', ['js']);

  // Fonts
  gulp.watch(dirs.source + '/fonts/*.{ttf,woff,woff2,eot,svg}', ['copy:fonts']);

  // Videos
  gulp.watch(dirs.source + '/videos/*.{mp4,ogv,webm}', ['copy:videos']);

  // CSS
  if (projectConfig.copiedCss.length) {
    gulp.watch(projectConfig.copiedCss, ['copy:css']);
  }

  // Javascript
  if (lists.js.length) {
    gulp.watch(lists.js, ['watch:js']);
  }

  // Images
  if (lists.img.length) {
    gulp.watch(lists.img, ['watch:images']);
  }

  // SVG Sprite
  if ((projectConfig.blocks['sprite-svg']) !== undefined) {
    gulp.watch(dirs.source + dirs.blocksDirName + 'sprite-svg/svg/*.svg', ['watch:sprite:svg']);
  }

  // PNG Sprite
  if ((projectConfig.blocks['sprite-png']) !== undefined) {
    gulp.watch(dirs.source + dirs.blocksDirName + '/sprite-png/png/*.png', ['watch:sprite:png']);
  }

});



gulp.task('watch:html', ['html'], reload);
gulp.task('watch:js', ['js'], reload);
gulp.task('watch:images', ['copy:images'], reload);
gulp.task('watch:sprite:svg', ['sprite:svg'], reload);
gulp.task('watch:sprite:png', ['sprite:png'], reload);



gulp.task('build', function (callback) {
  gulpSequence(
    'clean',
    ['sprite:svg', 'sprite:png'],
    ['style', 'copy:images', 'copy:fonts', 'copy:css', 'copy:js', 'js'],
    'html',
    callback
  );
});



gulp.task('default', ['serve']);


function getFilesList(config) {

  let res = {
    'css': [],
    'js': [],
    'img': [],
    'pug': [],
    'blocksDirs': []
  };

  // Style
  for (let blockName in config.blocks) {

    var blockPath = config.directories.source + config.directories.blocksDirName + '/' + blockName + '/';

    res.pug.push('../' + config.directories.blocksDirName + '/' + blockName + '/' + blockName + '.pug');
    if (config.blocks[blockName].length) {
      config.blocks[blockName].forEach(function(elementName) {
        res.pug.push(blockPath + blockName + elementName + '.pug');
      });
    }

    res.css.push(blockPath + blockName + '.scss');
    if (config.blocks[blockName].length) {
      config.blocks[blockName].forEach(function(elementName) {
        res.css.push(blockPath + blockName + elementName + '.scss');
      });
    }

  // JS
    res.js.push(blockPath + blockName + '.js');
    if (config.blocks[blockName].length) {
      config.blocks[blockName].forEach(function(elementName) {
        res.js.push(config.directories.srcPath + config.directories.blocksDirName + '/' + blockName + '/' + blockName + elementName + '.js');
      });
    }

    // Images
    res.img.push(config.directories.source + config.directories.blocksDirName + '/' + blockName + '/img/*.{jpg,jpeg,gif,png,svg}');

    res.blocksDirs.push(config.directories.blocksDirName + '/' + blockName + '/');
  }

  res.css = res.css.concat(config.addCssAfter);
  res.css = config.addCssBefore.concat(res.css);
  res.js = res.js.concat(config.addJsAfter);
  res.js = config.addJsBefore.concat(res.js);
  res.img = config.addImages.concat(res.img);

  return res;
}